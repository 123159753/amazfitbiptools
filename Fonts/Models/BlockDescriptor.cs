﻿namespace Fonts.Models
{
    public class BlockDescriptor
    {
        public char StartSymbol { get; set; }
        public char EndSymbol { get; set; }
        public int Offset { get; set; }
    }
}